package net.techU.kafkaRecomendaciones.Controllers;

import net.techU.kafkaRecomendaciones.Models.Recomendacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RecomendacionController {

    @Autowired
    private KafkaTemplate<Object, Object> template;

    @PostMapping("/kafka/recomendacion")
    public void envioData(@RequestBody Recomendacion mensaje){


        this.template.send("sugerenciasClientes", mensaje);
    }
}
