package net.techU.kafkaRecomendaciones.Models;


import java.util.List;

public class Recomendacion {

    public String idCliente;
    public String nombreConvenio;
    public List<Promocion> promocions;

    public Recomendacion() {
    }

    public Recomendacion(String idCliente, String nombreConvenio, List<Promocion> promocions) {
        this.idCliente = idCliente;
        this.nombreConvenio = nombreConvenio;
        this.promocions = promocions;
    }

}
